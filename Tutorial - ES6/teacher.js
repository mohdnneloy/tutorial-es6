const Person = require( "./person.js"); // "./ means in the present folder"

function promote(){
    console.log("Hello");
}

class Teacher extends Person.Person2{ // "default" tells that teacher is the default class that we are going to import

    constructor(name, degree){
        super(name);
        this.degree = degree;
    }
    teach(){
        console.log("Teach");
    }
}

module.exports = Teacher;
module.exports.promote = promote; 