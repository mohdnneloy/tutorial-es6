const Teacher = require( "./teacher.js");

// Default -> import ... from '';
// Name -> import {...} from '';

const teacher = new Teacher("Neloy", "BSc");
const promote = Teacher.promote;

console.log(teacher.name, teacher.degree);
teacher.talk();
promote();