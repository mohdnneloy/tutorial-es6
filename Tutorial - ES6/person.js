class Person{

    constructor(name){
        this.name = name;
    }
    walk(){
        console.log("walk");
    }
}

class Person2{

    constructor(name){
        this.name = name;
    }
    talk(){
        console.log("talk");
    }
}

module.exports.Person = Person;
module.exports.Person2 = Person2;